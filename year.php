
<?php

/*
Three Steps to find a Leap Year
*/


/*
year % 4 == 0 -> Evenly Divisible By 4
*/


// initialize Start year
$year=1980;
$stopYear=2018;
//$timestamp = strtotime( time: '1980');
//$year = date( format: 'Y', $timestamp);

//initialize stop year

//$timestamp = strtotime( time: '2018');
//$stopYear = date( format: 'Y', $timestamp);

//Initialize Leap Year Counter
$counter = 0;

//Run a while Loop TO Print Year
while ($year <= $stopYear) {
    
    if(isLeapYear($year)) {
        
        $counter = $counter + 1;
        echo $year  . 'Leap Year' .  '<br>'; 
    }
    else
        echo $year . '<br>';
    
    //Increment Year
    $year = $year + 1;
}



/* Echo Total Number of Years*/
echo '<br>'. 'Total number of years= '.$counter;

/*
Function to determine A leap Year
*/
function isLeapYear($year) {
    
    $isLeapYear = false;
    if ( (($year % 4)==0 && ($year % 100)!=0) || ($year % 400)==0 )
        $isLeapYear = true;
    return $isLeapYear;
}
